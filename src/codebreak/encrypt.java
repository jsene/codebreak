package codebreak;

import java.util.ArrayList;
import static java.util.Arrays.sort;
import java.util.Random;

/**
 *
 * @author Jacob Senecal
 */
public class encrypt {
    //tab holds the vigenere table used to encode and decode messages
    private char[][] tab;
    
    //keyword holds the word selected by the user to be the key
    private String keyword;
    
    //population holds the candidate solutions
    String[] population;
    private int keyLength;
    
    //Number of generations to run
    private int numGen;
    
    //rowLocate and colLocate are used for locating the equivalent ciphertext character
    //for a plaintext character and vice versa in the Vigenere Table
    char[] rowLocate = new char[26];
    char[] colLocate = new char[26];
    
    public encrypt(String key, int inNumGen, int popSize) {
       tab = new char[26][26];
       keyword = key;
       keyLength = key.length();
       numGen = inNumGen;
       population = new String[popSize];
    }
    
    public void execute(String inMessage) {
        keyGen();
        String a = encrypter(inMessage);

        for (int i = 0; i < numGen; i++) {
            crossOver(a);
            //nextGen(a); nextGen is an alternative method for creating new generations, I didn't get it working well.
        }
        
        //Output top ten solutions.
        System.out.println("The top ten solutions are:");
        for (int i = 0; i < 10; i++) {
            System.out.println(population[i]);
        }
        
        //Show the top solution.
        System.out.println("\n" + "The top solution is: " + finalAssesment(a));
    }
    
    //Create Vigenere Table, used for encrypting and decrypting messages.
    public void create() {
        int curLetter = 0;
        int letterTracker = 1;
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        
        //Make locating arrays
        for (int i = 0; i < 26; i++) {
            rowLocate[i] = alpha.charAt(i);
            colLocate[i] = alpha.charAt(i);
        }

        for (int row = 0; row < 26; row++) {
            for (int column = 0; column < 26; column++) {
                if (curLetter <= 25) {
                    tab[row][column] = alpha.charAt(curLetter);
                    curLetter += 1;
                } else {
                    curLetter = 0;
                    tab[row][column] = alpha.charAt(curLetter);
                    curLetter += 1;
                }
            }
            curLetter = letterTracker;
            letterTracker += 1;
        }
    }
    
    //Create the next gen of solutions by combining the solutions with the highest fitness
    private void crossOver(String inMessage) {
        Random r = new Random();
        double[] fitnessRating = new double[population.length];
        double[] newGenFitnessRating = new double[population.length*2];
        double[] temp = new double[population.length];
        double[] newGenTemp = new double[population.length*2];
        ArrayList<String> newPop = new ArrayList<>();
        ArrayList<String> sortedPop = new ArrayList<>();
        
        //Assess the fitness of each solution
        for (int i = 0; i < population.length; i++) {
            String decryptedMessage = decrypt(inMessage, population[i]);
            double fitnessValue = getFit(decryptedMessage);
            fitnessRating[i] = fitnessValue;
            temp[i] = fitnessValue;
        }
        
        //Sort the population so that the top half of solutions are at the front of an ArrayList 
        //and the bottom half are at the end of an ArrayList.
        sort(temp);
        double cutoff = temp[(temp.length/2)-1];
        
        for (int k = 0; k < population.length; k++) {
            if (fitnessRating[k] <= cutoff) {
                sortedPop.add(0, population[k]);
            } else {
                sortedPop.add(population[k]);
            } 
        }
        
        //Create half the children by crossover
        int curIndex = 0;
        while (curIndex <= population.length - 2) {
            int divide = r.nextInt(keyword.length());
            String crossOne = sortedPop.get(curIndex).substring(0, divide);
            crossOne += sortedPop.get(curIndex+1).substring(divide, keyword.length());
            newPop.add(crossOne);
            
            String crossTwo = sortedPop.get(curIndex+1).substring(0, divide);
            crossTwo += sortedPop.get(curIndex).substring(divide, keyword.length());
            newPop.add(crossTwo);      
            
            curIndex += 2;
        }
        
        //Create half the children by mutation
        for (int i = 0; i < population.length/2; i++) {
            int index = r.nextInt(newPop.size());
            String newSolution = mutate(newPop.get(index));
            newPop.add(newSolution);
        }
   
        //mutate the offspring
        for (int g = 0; g < newPop.size()/5; g++) {
            int selection = r.nextInt(newPop.size());
            newPop.set(selection, mutate(newPop.get(selection)));
        }
      
        //Assess the fitness of the new generation.
        for (int i = 0; i < newPop.size(); i++) {
            String decryptedMessage = decrypt(inMessage, newPop.get(i));
            double fitnessValue = getFit(decryptedMessage);
            newGenFitnessRating[i] = (fitnessValue);
            newGenTemp[i] = fitnessValue;
        }
        
        //Sort the new generation so that the top half of solutions are selected
        sort(newGenTemp);
        sortedPop.clear();
        double newCutoff = newGenTemp[(newGenTemp.length/2)-1];
        for (int k = 0; k < newPop.size(); k++) {
            if (newGenFitnessRating[k] <= newCutoff) {
                sortedPop.add(0, newPop.get(k));
            } else {
                sortedPop.add(newPop.get(k));
            }
        }
        
        //Store top solutions in population[]
        for (int a = 0; a < population.length; a++) {
            population[a] = sortedPop.get(a);
        }
    }
    
    //Alternative method for creating a new generation, using mutation rather than crossing two parents
    public void nextGen(String inMessage) {
        ArrayList<String> children = new ArrayList<>();
        ArrayList<String> sortedChildren = new ArrayList<>();
        int newPopSize = population.length * 2;
        
        while (newPopSize > 0) {
            if (newPopSize > population.length * 2 * 0.5) {
                for (int i = 0; i < population.length * 0.1; i++) {
                    children.add(mutate(population[i]));
                    newPopSize--;
                }
            } else if (newPopSize > population.length * 2 * 0.1) {
                for (int j = (int) (population.length * 0.1) + 1; j < population.length * 0.4; j++) {
                    children.add(mutate(population[j]));
                    newPopSize--;
                }
            } else {
                for (int k = (int) (population.length * 0.4) + 1; k < population.length; k++) {
                    children.add(mutate(population[k]));
                    newPopSize--;
                }
            }
        }
        
        //Assess fitness of new generation
        double[] fitness = new double[children.size()];
        double[] temp = new double[children.size()];
        
        for (int i = 0; i < children.size(); i++) {
            String decryptedMessage = decrypt(inMessage, children.get(i));
            double fitnessValue = getFit(decryptedMessage);
            fitness[i] = fitnessValue;
            temp[i] = fitnessValue;
        }
        
        //Select fittest children for next generation
        sort(temp);
        double cutOff = temp[(temp.length)/2 - 1];
        
        for (int i = 0; i < population.length; i++) {
            if (fitness[i] < cutOff) {
                sortedChildren.add(0, children.get(i));
            } else {
                sortedChildren.add(children.get(i));
            }
        }
        
        //Populate the solutions array
        for (int i = 0; i < population.length; i++) {
            population[i] = sortedChildren.get(i);
        }
    }
    
    //Randomly change one letter in a candidate solution
    private String mutate(String candidate) {
        Random r = new Random();
        String mutatedCandidate = "";
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        String[] holder = new String[candidate.length()];
        int position = r.nextInt(candidate.length());
        char newChar = alpha.charAt(r.nextInt(26));
        
        for (int i = 0; i < candidate.length(); i++) {
            holder[i] = String.valueOf(candidate.charAt(i));
        }
        
        holder[position] = String.valueOf(newChar);
        for (String c : holder) {
            mutatedCandidate += c;
        }
        return mutatedCandidate;
    }
    
    private String finalAssesment(String inMessage) {
        //Find the best solution
        double[] fitnessRating = new double[population.length];
        double finalFit = 5;
        String finalSoln = null;
        
        for (int i = 0; i < population.length; i++) {
            String decryptedMessage = decrypt(inMessage, population[i]);
            double fitnessValue = getFit(decryptedMessage);
            fitnessRating[i] = fitnessValue;
        }
        
        for (int j = 0; j < fitnessRating.length; j++) {
            if (fitnessRating[j] < finalFit) {
                finalFit = fitnessRating[j];
                finalSoln = population[j];
            }
        }
        return finalSoln;
    }
    
    //Method for assessing the fitness of a solution.
    private double getFit(String inMessage) {
        String[] messageHolder = new String[inMessage.length()];
        int[] letterCounter = new int[26];
        double[] frequency = new double[26];
        for (int i = 0; i < inMessage.length(); i++) {
            messageHolder[i] = String.valueOf(inMessage.charAt(i));
        }
        
        for(String c : messageHolder) {
            switch (c) {
                case "a": letterCounter[0] += 1;
                    break;
                case "b": letterCounter[1] += 1;
                    break;
                case "c": letterCounter[2] +=1;
                    break;
                case "d": letterCounter[3] +=1;
                    break;
                case "e": letterCounter[4] +=1;
                    break;
                case "f": letterCounter[5] +=1;
                    break;
                case "g": letterCounter[6] +=1;
                    break;
                case "h": letterCounter[7] +=1;
                    break;
                case "i": letterCounter[8] +=1;
                    break;
                case "j": letterCounter[9] +=1;
                    break;
                case "k": letterCounter[10] +=1;
                    break;
                case "l": letterCounter[11] +=1;
                    break;
                case "m": letterCounter[12] +=1;
                    break;
                case "n": letterCounter[13] +=1;
                    break;
                case "o": letterCounter[14] +=1;
                    break;
                case "p": letterCounter[15] +=1;
                    break;
                case "q": letterCounter[16] +=1;
                    break;
                case "r": letterCounter[17] +=1;
                    break;
                case "s": letterCounter[18] +=1;
                    break;
                case "t": letterCounter[19] +=1;
                    break;
                case "u": letterCounter[20] +=1;
                    break;
                case "v": letterCounter[21] +=1;
                    break;
                case "w": letterCounter[22] +=1;
                    break;
                case "x": letterCounter[23] +=1;
                    break;
                case "y": letterCounter[24] +=1;
                    break;
                case "z": letterCounter[25] +=1;
                    break;
            }
        }
        
        for (int j = 0; j < letterCounter.length; j++) {
            frequency[j] = (double) letterCounter[j]/inMessage.length();
        }
        
        double uniGramFreq = compareFreq(frequency);
        bigram bigramFreq = new bigram();
        double biGramFreq = bigramFreq.findBigram(inMessage);
        
        return uniGramFreq + biGramFreq;
    }
    
    //The input for this method is the measured frequency of the decrypted message.
    //This frequency is then compared to the known frequency of letters in English.
    private double compareFreq(double[] measuredFreq) {
        double F = 0;
        double[] knownFreq = {0.0812, 0.0149, 0.0271, 0.0432, 0.1202, 0.0230, 0.0203, 0.0592, 0.0731, 0.0010, 0.0069, 0.0398, 0.0261, 0.0695, 0.0768, 0.0182, 0.0011, 0.0602, 0.0628, 0.0910, 0.0288, 0.0111, 0.0209, 0.0017, 0.0211, 0.0007};
        
        for (int i = 0; i < measuredFreq.length; i++) {
             F += (knownFreq[i] - measuredFreq[i]) * (knownFreq[i] - measuredFreq[i]);
        }
        return F*0.7; 
    }
    
    //Randomly generate initial candidate solutions with length equal to keyword length.
    private void keyGen() {
        Random r = new Random();
        String curKey = "";
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < population.length; i++) {
            for (int j = 0; j < keyword.length(); j++) {
                curKey = curKey + String.valueOf(alpha.charAt(r.nextInt(26)));
            }
            population[i] = curKey;
            curKey = "";
        }
    }
    
    //This method encrypts the original message.
    private String encrypter(String inMessage) {
        String[] messageHolder = new String[inMessage.length()];
        String[] keyHolder = new String[inMessage.length()];
        String[] encryptedMessage = new String[inMessage.length()];
        int keyTracker = 0;
        String check = "";
        
        //Move plaintext message character by character into an array
        for (int i = 0; i < inMessage.length(); i++) {
            messageHolder[i] = String.valueOf(inMessage.charAt(i));
        }
        
        //Each letter in the keyword will correspond to a letter in the message. The keyword will be repeated 
        //until the length of the message is reached.
        for (int j = 0; j < inMessage.length(); j++) {
            if (keyTracker <= keyword.length() - 1) {
                keyHolder[j] = String.valueOf(keyword.charAt(keyTracker));
                keyTracker += 1;
            } else {
                keyTracker = 0;
                keyHolder[j] = String.valueOf(keyword.charAt(keyTracker));
                keyTracker += 1;
            }
        }
        
        //Substitute plaintext characters for ciphertext characters
        String keyLetter = "";
        String plainLetter = "";
        int rowNum = 0;
        int colNum = 0;
        for (int k = 0; k < inMessage.length(); k++) {
            keyLetter = keyHolder[k];
            plainLetter = messageHolder[k];
            for (int l = 0; l < rowLocate.length; l++) {
                if (String.valueOf(rowLocate[l]).equals(keyLetter)) {
                    rowNum = l;
                }
                if (String.valueOf(colLocate[l]).equals(plainLetter)) {
                    colNum = l;
                }
            }
            encryptedMessage[k] = String.valueOf(tab[rowNum][colNum]);
            check = check + encryptedMessage[k];
        }
        return check;
    }
    
    //This method decrypts a message with a given solution. 
    private String decrypt(String inMessage, String inKey) {
        String[] messageHolder = new String[inMessage.length()];
        String[] keyHolder = new String[inMessage.length()];
        String[] decryptedMessage = new String[inMessage.length()];
        int keyTracker = 0;
        String check = "";

        //Move ciphertext message character by character into an array
        for (int i = 0; i < inMessage.length(); i++) {
            messageHolder[i] = String.valueOf(inMessage.charAt(i));
        }

        //Each letter in the keyword will correspond to a letter in the message. The keyword will be repeated 
        //until the length of the message is reached.
        for (int j = 0; j < inMessage.length(); j++) {
            if (keyTracker <= inKey.length() - 1) {
                keyHolder[j] = String.valueOf(inKey.charAt(keyTracker));
                keyTracker += 1;
            } else {
                keyTracker = 0;
                keyHolder[j] = String.valueOf(inKey.charAt(keyTracker));
                keyTracker += 1;
            }
        }

        //Substitute ciphertext characters for plaintext characters
        String keyLetter = "";
        int rowNum = 0;
        int colNum = 0;
        
        //For each letter in the message
        for (int k = 0; k < inMessage.length(); k++) {
            keyLetter = keyHolder[k];
            
            //Use the current keyword letter to find the column of the ciphertext letter
            for (int l = 0; l < rowLocate.length; l++) {
                if (String.valueOf(colLocate[l]).equals(keyLetter)) {
                    colNum = l;
                }
            }
            
            //Look through the rows of the column found above to find the ciphertext letter
            for (int f = 0; f < 26; f++) {
                if (String.valueOf(tab[f][colNum]).equals(messageHolder[k])) {
                    rowNum = f;
                }
            }
            
            //The index of the row found above is the decrypted letter
            decryptedMessage[k] = String.valueOf(rowLocate[rowNum]);
            check = check + decryptedMessage[k];
        }
        return check;
    }
    
    //This method may be used to print out a visual representation of the Vigenere Table.
    public void print() {
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++) {
                System.out.print(tab[i][j] + " ");
            }
            System.out.println();
        }
    }
}
