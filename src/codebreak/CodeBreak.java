/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codebreak;

/**
 *
 * @author Jacob Senecal
 */
public class CodeBreak {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /**
         * I have called the Congress into extraordinary session because there are serious, very serious, choices of policy to be
         * made and made immediately, which it was neither right nor constitutionally permissible that I should assume the responsibility
         * of making on the third of February last, I officially laid before you the extraordinary announcement of the imperial German
         * government that on and after the first day of February it was its purpose to put aside all restraints of law or of humanity and
         * use its submarines to sink every vessel that sought to approach either the ports of Great Britain and Ireland, or the western
         * coast of Europe, or any of the ports controlled by the enemies of Germany within the Mediterranean.
         */
        String message = "ihavecalledthecongressintoextraordinarysessionbecausethereareseriousveryseriouschoicesofpolicytobemadeandmade" + 
                         "immediatelywhichitwasneitherrightnorconstitutionallypermissiblethatishouldassumetheresponsibilityof" +
                         "makingonthethirdoffebruarylastiofficiallylaidbeforeyoutheextraordinaryannouncementoftheimperialgerman" +
                         "governmentthatonandafterthefirstdayoffebruaryitwasitspurposetoputasideallrestraintsoflaworofhumanityand" +
                         "useitssubmarinestosinkeveryvesselthatsoughttoapproacheithertheportsofgreatbritainandirelandorthewestern" +
                         "coastsofeuropeoranyoftheportscontrolledbytheenemiesofgermanywithinthemediterranean";
        
        //encrypt("keyword", number of generations, solutions population size)
        encrypt test = new encrypt("president", 200, 20);
        test.create();
        test.execute(message);
    }
    
}
