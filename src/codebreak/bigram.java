/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codebreak;


/**
 *
 * @author Jacob Senecal
 */
public class bigram {
        public double findBigram(String inMessage) {
        String[] knownBigram = {"th", "he", "in", "er", "an", "re", "on", "at", "en", "nd", "ti", "es", "or", "te", "of", "ed", "is", "it", "al", "ar", "st", "to", "nt", "ng", "se", "ha", "as", "ou", "io"};
        double[] knownFreq = {0.0356, 0.0307, 0.0243, 0.0205, 0.0199, 0.0185, 0.0176, 0.0149, 0.0145, 0.0135, 0.0134, 0.0134, 0.0128, 0.012, 0.0117, 0.0117, 0.0113, 0.0112, 0.0109, 0.0107, 0.0105, 0.0104, 0.0104, 0.0095, 0.0093, 0.0093, 0.0087, 0.0087, 0.0083};
        int[] counter = new int[knownBigram.length];
        String[] messageHolder = new String[inMessage.length()];
        double[] frequency = new double[counter.length];
        int bigramsFound = 0;
        
        for (int i = 0; i < inMessage.length(); i++) {
            messageHolder[i] = String.valueOf(inMessage.charAt(i));
        }
        
        String temp2 = "";
        for (int j = 0; j < messageHolder.length; j++) {
            for (int k = 0; k < knownBigram.length; k++) {
                if (j <= messageHolder.length-2) {
                    temp2 = messageHolder[j] + messageHolder[j+1];
                }
                if (temp2.equals(knownBigram[k])) {
                    counter[k] += 1;
                    bigramsFound += 1;
                }
                temp2 = "";
            }
        }
        
        for (int k = 0; k < frequency.length; k++) {
            if (bigramsFound != 0) {
                frequency[k] = counter[k]/bigramsFound;
            } else {
                frequency[k] = knownFreq[k];
            }
        }       
        
        double F = 0;
        for (int l = 0; l < frequency.length; l++) {
            F += (knownFreq[l] - frequency[l]) * (knownFreq[l] - frequency[l]);
        }
        
        return F*0.3;
    }
}
